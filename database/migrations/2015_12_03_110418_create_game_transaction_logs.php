 <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameTransactionLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_transaction_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_profile_id')->unsigned();
            $table->integer('transaction_id')->unsigned()->unique();
            $table->string('from');
            $table->string('to');
            $table->timestamps();

            $table->foreign('game_profile_id')
                  ->references('id')
                  ->on('user_game_profiles');

            $table->foreign('transaction_id')
                  ->references('id')
                  ->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('game_transaction_logs');
    }
}
