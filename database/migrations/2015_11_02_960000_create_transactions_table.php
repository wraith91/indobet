<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('trxn_type_id')->unsigned();
            $table->integer('trxn_status_id')->unsigned();
            $table->integer('bank_id')->unsigned();
            $table->integer('game_id')->unsigned()->nullable();
            $table->double('amount');
            $table->string('remarks')->nullable();
            $table->string('modify_by')->nullable();
            $table->timestamp('completed_at')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('trxn_status_id')
                ->references('id')
                ->on('transaction_status');

            $table->foreign('trxn_type_id')
                ->references('id')
                ->on('transaction_types');

            $table->foreign('bank_id')
                ->references('id')
                ->on('user_bank_profiles');

            $table->foreign('game_id')
                ->references('id')
                ->on('game_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
    }
}
