<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_profile_id')->unsigned();
            $table->integer('game_type_id')->unsigned();
            $table->integer('bonus_status_id')->unsigned();
            $table->integer('bonus_point')->unsigned();
            $table->string('balance', 30)->nullable();
            $table->integer('file_log_id')->unsigned();
            $table->timestamps();

            $table->foreign('game_profile_id')
                  ->references('id')
                  ->on('user_game_profiles')
                  ->onDelete('cascade');

            $table->foreign('game_type_id')
                  ->references('id')
                  ->on('game_types');

            $table->foreign('bonus_status_id')
                  ->references('id')
                  ->on('bonus_status');

            $table->foreign('file_log_id')
                  ->references('id')
                  ->on('bonus_file_logs')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bonuses');
    }
}
