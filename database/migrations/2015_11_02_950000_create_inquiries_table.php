<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inquiry_type_id')->unsigned();
            $table->integer('inquiry_status_id')->unsigned();
            $table->string('name');
            $table->string('email');
            $table->string('subject', 50);
            $table->string('body');
            $table->timestamps();

            $table->foreign('inquiry_type_id')
                ->references('id')
                ->on('inquiry_types');
                
            $table->foreign('inquiry_status_id')
                ->references('id')
                ->on('inquiry_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inquiries');
    }
}
