<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusFileLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus_file_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('game_type_id')->unsigned();
            $table->timestamps();

            $table->foreign('game_type_id')
              ->references('id')
              ->on('game_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bonus_file_logs');
    }
}
