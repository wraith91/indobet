<?php

use App\Agenwin\TransactionType;
use Illuminate\Database\Seeder;

class TransactionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction_types')->delete();

        TransactionType::create([
        	'name' => 'Deposit'
        ]);

        TransactionType::create([
        	'name' => 'Withdraw'
        ]);
    }
}
