<?php

use App\Agenwin\GameType;
use Illuminate\Database\Seeder;

class GameTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
  			DB::table('game_types')->delete();

        GameType::create([
        	'name' => 'SBOBET'
        ]);

        GameType::create([
        	'name' => 'IBCBET'
        ]);

        GameType::create([
        	'name' => 'CALIBET'
        ]);

        GameType::create([
            'name' => 'KLIKPOKER'
        ]);
    }
}
