<?php

use App\Agenwin\BankType;
use Illuminate\Database\Seeder;

class BankTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * @return void
     */
    public function run()
    {
		DB::table('bank_types')->delete();

        BankType::create([
        	'name' => 'BNI'
        ]);

        BankType::create([
        	'name' => 'BRI'
        ]);

        BankType::create([
        	'name' => 'BCA'
        ]);

        BankType::create([
        	'name' => 'Mandiri'
        ]);
    }
}
