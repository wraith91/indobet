<?php

use App\Agenwin\BonusStatus;
use Illuminate\Database\Seeder;

class BonusStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bonus_status')->delete();

        BonusStatus::create([
            'name' => 'Not Transferred '
        ]);

        BonusStatus::create([
        	'name' => 'Transferred'
        ]);
    }
}
