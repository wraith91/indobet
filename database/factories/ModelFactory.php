<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Agenwin\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->firstName,
        'password' => '12345678',
        'email' => $faker->email,
        'is_active' => 0,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Agenwin\UserProfile::class, function (Faker\Generator $faker) {
    return [
        'user_id' => $user,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName, 
        'birth_date' => $faker->dateTimeThisCentury->format('Y-m-d'),
        'gender' => 'Male',
        'mobile' => $faker->phoneNumber
    ];
});

$factory->define(App\Agenwin\UserBankProfile::class, function (Faker\Generator $faker) {
    return [
        'user_id' => factory(App\Agenwin\User::class)->create()->id,
        'bank_type_id' => 1,
        'account_name' => $faker->name,
        'account_no' => $faker->randomNumber(8), 
    ];
});

$factory->define(App\Agenwin\UserGameProfile::class, function (Faker\Generator $faker) {
    return [
        'user_id' => factory(App\Agenwin\User::class)->create()->id,
        'game_id' => 1,
        'username' => $faker->name,
        'password' => $faker->randomNumber(8), 
    ];
});

// $user = factory('App\Agenwin\User', 2)->create()->each(function($u) { $u->profile()->save(factory('App\Agenwin\UserProfile')->create()); $u->bankAccount()->save(factory('App\Agenwin\UserBankProfile')->create()); $u->gameAccount()->save(factory('App\Agenwin\UserGameProfile')->create());});

// $user->assignRole('member');

// $user = factory('App\Agenwin\User')->create()->each(function($u) { $u->profile()->save(factory('App\Agenwin\UserProfile')->create()); $u->bankAccount()->save(factory('App\Agenwin\UserBankProfile')->create()); $u->gameAccount()->save(factory('App\Agenwin\UserGameProfile')->create());});
