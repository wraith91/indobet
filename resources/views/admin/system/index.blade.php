@extends('layouts.admin.master')

@section('title')
	System: User
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', [
    'title' => 'System User',
    'secondary_menu' => ['Create User' => 'admin/system/user/create']
  ])
  
  @include('partials.flash')

	<div class="block block-default">
    <div class="block-title"><h4>Authorize User Lists:</h4></div>
    <table class="information-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>System ID</th>
          <th>Username</th>
          <th>Last Login</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
				@foreach ($users as $user)
          @unless($user->hasRole('admin'))
	        <tr>
            <td>{{ $i++ }}</td>
            <td>{!! 'CS' . str_pad($user->id, 5, "0", STR_PAD_LEFT) !!}</td>
            <td>{{ $user->username }}</td>
            <td>{{ $user->updated_at->diffForHumans() }}</td>
            <td>
              {!! Form::open(['method' => 'DELETE', 'url' => ['/admin/system/user', $user->id] ]) !!}
                <a href="{{ '/admin/system/user/' . $user->id . '/edit' }}" class="form-button default">Edit</a>
                {!! Form::submit('Delete', ['class' => 'form-button danger']) !!}  
              {!! Form::close() !!}
            </td>
	        </tr>
          @endif
				@endforeach
      </tbody>
    </table>
  </div>
@stop
