@extends('layouts.admin.master')

@section('title')
	System: User
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', [
    'title' => 'System User',
    'secondary_menu' => ['Back' => 'admin/system/user']
  ])

	@include('errors.list')

	@include ('partials.flash')

	<div class="block-group">
		{!! Form::open(['url' => 'admin/system/user']) !!}
		<div class="col-6 block">
    	<div class="block-title"><h4>Account Details:</h4></div>
	  	<div class="field-wrapper-block lg">
	      {!! Form::label('role', 'Role:') !!}
	     {!! Form::select('role', $roles, '2',['class' => 'form-select']) !!}
	  	</div>
	  	<div class="field-wrapper-block lg">
	      {!! Form::label('username', 'Username:') !!}
	      {!! Form::text('username', null, ['class' => 'form-input']) !!}
	  	</div>	  	
	  	<div class="field-wrapper-block lg">
	      {!! Form::label('password', 'Password:') !!}
	      {!! Form::password('password', ['class' => 'form-input']) !!}
	  	</div>	  	
	  	<div class="field-wrapper-block lg">
	      {!! Form::label('confirm_password', 'Confirm Password:') !!}
	      {!! Form::password('confirm_password', ['class' => 'form-input']) !!}
	  	</div>	  	
	  	<div class="field-wrapper-block lg">
	      {!! Form::label('email', 'Email:') !!}
	      {!! Form::text('email', null, ['class' => 'form-input']) !!}
	  	</div>
		</div>
		<div class="col-6 block">
    	<div class="block-title"><h4>Personal Information:</h4></div>
	  	<div class="field-wrapper-block lg">
	      {!! Form::label('first_name', 'First Name:') !!}
	      {!! Form::text('first_name', null, ['class' => 'form-input']) !!}
	  	</div>	  	
	  	<div class="field-wrapper-block lg">
	      {!! Form::label('last_name', 'Surname:') !!}
	      {!! Form::text('last_name', null, ['class' => 'form-input']) !!}
	  	</div>
	  	<div class="field-wrapper-block lg">
	      {!! Form::label('gender', 'Gender:') !!}
	     {!! Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], null,['class' => 'form-select']) !!}
	  	</div>
	  	<div class="field-wrapper-block lg">
	      {!! Form::label('mobile', 'Mobile:') !!}
	      {!! Form::text('mobile', null, ['class' => 'form-input']) !!}
	  	</div>
	  	<div class="field-wrapper-block lg">
	  		{!! Form::submit('Save', ['class' => 'form-button active']) !!}
	  	</div>
		</div>
		{!! Form::close() !!}
  </div>
@stop
