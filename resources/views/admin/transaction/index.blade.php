@extends('layouts.admin.master')

@section('title')
	Transactions
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', [
		'title' => 'Transactions', 
		'secondary_menu' => 
			[
				'Deposit' => 'admin/transactions/deposit', 
				'Withdraw' => 'admin/transactions/withdraw'
			]
		])
@stop