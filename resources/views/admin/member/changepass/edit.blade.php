@extends('layouts.admin.master')

@section('title')
	Member: Account
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', [
    'title' => 'Member Details',
    'secondary_menu' => 
      [
        'Back' => 'admin/members/lists'
      ]
  ])

	@include('errors.list')

	@include ('partials.flash')

	<div class="block-group">
		<div class="col-6">
			{!! Form::model($user->bankAccount, ['method' => 'PATCH', 'url' => ['admin/members/bankaccount', $user->id]]) !!}
			<div class="block">
	    	<div class="block-title"><h4>Bank Details: <b>{{ $user->profile->first_name . ' ' . $user->profile->last_name }}</b></h4></div>
{{--         <div class="field-wrapper-block lg">
            {!! Form::label('bank_name', 'Bank name *') !!}
            {!! Form::select('bank_name', $bank_name, null, ['class' => 'form-select']) !!}
        </div> --}}
        <div class="field-wrapper-block lg">
            {!! Form::label('account_name', 'Account name *') !!}
            {!! Form::text('account_name', null, ['class' => 'form-input', 'required' => 'required']) !!}
        </div>
        <div class="field-wrapper-block lg">
            {!! Form::label('account_no', 'Account number *') !!}
            {!! Form::text('account_no', null, ['class' => 'form-input', 'required' => 'required']) !!}
        </div>
		  	<div class="field-wrapper-block lg">
		  		{!! Form::submit('Modify', ['class' => 'form-button active']) !!}
		  	</div>
			</div>
			{!! Form::close() !!}
		</div>
		<div class="col-6">
			{!! Form::open(['method' => 'PATCH', 'url' => ['admin/members', $user->id]]) !!}
			<div class="block">
	    	<div class="block-title"><h4>Account Details</h4></div>
		  	<div class="field-wrapper-block lg">
		      {!! Form::label('password', 'Password:') !!}
		      {!! Form::password('password', ['class' => 'form-input', 'required']) !!}
		  	</div>	  	
		  	<div class="field-wrapper-block lg">
		      {!! Form::label('confirm_password', 'Confirm Password:') !!}
		      {!! Form::password('confirm_password', ['class' => 'form-input', 'required']) !!}
		  	</div>
		  	<div class="field-wrapper-block lg">
		  		{!! Form::submit('Change Password', ['class' => 'form-button active']) !!}
		  	</div>
			</div>
			{!! Form::close() !!}
		</div>
  </div>
@stop
