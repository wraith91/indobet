@extends('layouts.admin.master')

@section('title')
	Members: Information
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', ['title' => 'Member Lists'])
  @include('errors.list')
  @include('partials.flash')
  <div class="block-group">
          <div class="field-wrapper-inline sm">
            {!! Form::label('search', 'Search:') !!}
            {!! Form::text('search', null, ['class' => 'form-input', 'id' => 'searchInput']) !!}
      </div>
  </div>
  <div class="block-group">
    <div class="block">
      <div class="block-title"><h4>Profile:</h4></div>
        <table class="information-table">
          <thead>
            <th>No.</th>
            <th>System ID</th>
            <th>Username</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Account Name</th>
            <th>Bank Account</th>
            <th>Bank Name</th>
            <th>Date of Registration</th>
            <th>Action</th>
          </thead>
          <tbody id="fbody">
            {{-- @if(Request::get('search') != null && $users != null) --}}
              @foreach ($users as $i => $user)
                <tr>
                  <td>{{ $i+1 }}</td>
                  <td>{{ 'IN' . str_pad($user->id, 5, "0", STR_PAD_LEFT) }}</td>
                  <td><a href="/admin/members/information?search={{ $user->username }}">{{ $user->username }}</a></td>
                  <td>{{ $user->profile->first_name }}</td>
                  <td>{{ $user->profile->last_name }}</td>
                  <td>{{ $user->email }}</td>
                  <td>{{ $user->bankAccount->account_name }}</td>
                  <td>{{ $user->bankAccount->account_no }}</td>
                  <td>{{ $user->bankAccount->bankName->name }}</td>
                  <td>{{ $user->created_at }}</td>
                  <td>                      
                      <a href="{!! '' . $user->id . '/edit'!!}" class="form-button active">Edit</a> 
                     <!--  {!! Form::open(['method' => 'DELETE', 'url' => ['admin/members', $user->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'form-button danger']) !!}
                      {!! Form::close() !!} -->
                  </td>
                </tr>
              @endforeach
            {{-- @endif --}}
          </tbody>
        </table>
  </div>
@stop

@section('footer')

  <script>
  $("#searchInput").keyup(function () {
    //split the current value of searchInput
    var data = this.value.split(" ");
    //create a jquery object of the rows
    var row = $("#fbody").find("tr");
    if (this.value == "") {
        row.show();
        return;
    }
    //hide all the rows
    row.hide();

    //Recusively filter the jquery object to get results.
    row.filter(function (i, v) {
        var $t = $(this);
        for (var d = 0; d < data.length; ++d) {
            if ($t.is(":contains('" + data[d] + "')")) {
                return true;
            }
        }
        return false;
    })
    //show the rows that match.
    .show();
    }).focus(function () {
        this.value = "";
        $(this).unbind('focus');
    });
  </script>

@stop