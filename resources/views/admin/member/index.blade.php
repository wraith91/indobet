@extends('layouts.admin.master')

@section('title')
	Members: Information
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', ['title' => 'Member Information'])
  @include('errors.list')
  @include('partials.flash')
  <div class="block-group">
    {!! Form::open(['method' => 'GET', 'url' => 'admin/members/information']) !!}
      <div class="field-wrapper-inline sm">
          {!! Form::label('search', 'Search:') !!}
          {!! Form::text('search', null, ['class' => 'form-input', 'placeholder' => 'Username of the user.']) !!}
      </div>
       <div class="field-wrapper-inline sm">
          {!! Form::submit('Search', ['class' => 'form-button active']) !!} 
       </div>
    {!! Form::close() !!}
  </div>
  <div class="block-group">
    <div class="block col-6">
      <div class="block-title"><h4>Profile:</h4></div>
      @if(Request::get('search') != null && $user != null)
        <table class="display-table">
          <tbody>
            <tr>
              <td>System ID:</td>
              <td>{{ 'IN' . str_pad($user->id, 5, "0", STR_PAD_LEFT) }}</td>
            </tr>
            <tr>
              <td>Username:</td>
              <td>{{ $user->username }}</td>
            </tr>
            <tr>
              <td>Real Name:</td>
              <td>{{ $user->profile->first_name . " " . $user->profile->last_name }}</td>
            </tr>
            <tr>
              <td>Email:</td>
              <td>{{ $user->email }}</td>
            </tr>
            <tr>
              <td>Date of Registration:</td>
              <td>{{ $user->created_at }}</td>
            </tr>
            <tr>
              <td>Outstanding Balance:</td>
              <td>{{ $max_balance }}</td>
            </tr>
            <tr>
              <td>Latest Transaction:</td>
              <td>{{ $latest_trxn }}</td>
            </tr>
          </tbody>
        </table>
      @endif
    </div> {{-- END OF PROFILE  --}}
    <div class="block col-6">
      <div class="block-title"><h4>Game Profile:</h4></div>
      @if(Request::get('search') != null && $user != null)
        <table class="information-table">
          <thead>
            <tr>
              <th>No.</th>
              <th>Game Type</th>
              <th>Username</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
        @foreach ($user->gameAccount as $i => $account)
          <tr>
            <td>{{ $i+1 }}</td>
            <td>{{ $account->gameType->name }}</td>
            <td>{{ $account->username }}</td>
            <td>
              <a href="{{ 'information/account/' . $account->gameType->name . '/' . $account->id }}" class="form-button active">Logs</a>
            </td>
          </tr>
        @endforeach
          </tbody>
        </table>
      @endif
    </div> {{-- END OF GAME PROFILE  --}}
  </div>
  <div class="block-group">
    <div class="block col-6">
      <div class="block-title"><h4>Deposit History:</h4></div>
      @if(Request::get('search') != null && $user != null)
        <table class="information-table">
          <thead>
            <tr>
              <th>No.</th>
              <th>Game Type</th>
              <th>Status</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
        @foreach ($deposits as $d => $deposit)
          <tr>
            <td>{{ $d+1 }}</td>
            <td>{{ $deposit->gameType->name }}</td>
            <td>{{ $deposit->status->name }}</td>
            <td>{{ $deposit->completed_at }}</td>
          </tr>
        @endforeach
          </tbody>
        </table>
      @endif
    </div> {{-- END OF DEPOSIT --}}
    <div class="block col-6">
      <div class="block-title"><h4>Withdraw History:</h4></div>
      @if(Request::get('search') != null && $user != null)
        <table class="information-table">
          <thead>
            <tr>
              <th>No.</th>
              <th>Game Type</th>
              <th>Status</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
        @foreach ($withdraws as $w => $withdraw)
          <tr>
            <td>{{ $w+1 }}</td>
            <td>{{ $withdraw->gameType->name }}</td>
            <td>{{ $withdraw->status->name }}</td>
            <td>{{ $withdraw->completed_at }}</td>
          </tr>
        @endforeach
          </tbody>
        </table>
      @endif
    </div> {{-- END OF WITHDRAW --}}
  </div> 
  <div class="block-group">
    <div class="block col-6">
      <div class="block-title"><h4>Deposit to Account:</h4></div>
      @if(Request::get('search') != null && $user != null)
      {!! Form::model($user->bankAccount, ['url' => 'transaction']) !!}
        {!! Form::hidden('trxn_type_id', '1') !!}
        {!! Form::hidden('trxn_user_id', $user->id) !!}
        <div class="field-wrapper-block sm">
                {!! Form::label('account_name', 'Account name') !!}
                {!! Form::text('account_name', null, ['class' => 'form-input', 'required' => 'required', 'readonly']) !!}
        </div><!-- deposit-form-component -->                           
        <div class="field-wrapper-block sm">
                {!! Form::label('account_no', 'Account number') !!}
                {!! Form::text('account_no', null, ['class' => 'form-input', 'required' => 'required', 'readonly']) !!}
        </div><!-- deposit-form-component -->
        <div class="field-wrapper-block sm">
                {!! Form::label('game_id', 'Select Game *') !!}
                {!! Form::select('game_id', $game, null, ['class' => 'form-select']) !!}
        </div><!-- deposit-form-component -->                           
        <div class="field-wrapper-block sm">
                {!! Form::label('amount', 'Amount *') !!}
                {!! Form::text('amount', null, ['class' => 'form-input', 'required' => 'required']) !!}
        </div><!-- deposit-form-component -->
        <div class="field-wrapper-block sm">
                {!! Form::submit('Submit', ['class' => 'form-button active']) !!}
        </div><!-- deposit-form-component -->
    {!! Form::close() !!}
    @endif
    </div> {{-- END OF DEPOSIT --}}
    <div class="block col-6">
      <div class="block-title"><h4>Withdraw to Account:</h4></div>
      @if(Request::get('search') != null && $user != null)
        {!! Form::open(['url' => 'transaction']) !!}
          {!! Form::hidden('trxn_type_id', '2') !!}
          {!! Form::hidden('trxn_user_id', $user->id) !!}
          <div class="field-wrapper-block sm">
              {!! Form::label('game_id', 'Select Game *') !!}
              {!! Form::select('game_id', $game, null, ['class' => 'form-select']) !!}
          </div><!-- deposit-form-component -->
          <div class="field-wrapper-block sm">
              {!! Form::label('amount', 'Amount *') !!}
              {!! Form::text('amount', null, ['class' => 'form-input', 'required' => 'required']) !!}
          </div><!-- deposit-form-component -->
          <div class="field-wrapper-block sm">
              {!! Form::submit('Submit', ['class' => 'form-button active']) !!}
          </div><!-- deposit-form-component -->
        {!! Form::close() !!}
      @endif
    </div> {{-- END OF WITHDRAW --}}
  </div> 
@stop