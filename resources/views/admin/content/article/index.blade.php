@extends('layouts.admin.master')

@section('title')
  Content: Articles
@stop

@section('content')
  @include('layouts.admin.secondary_header_menu', [
    'title' => 'Article Lists',
    'secondary_menu' => ['Create Article' => 'admin/article/create']
  ])
  @include('errors.list')
  @include('partials.flash')
  <div class="block-group">
          <div class="field-wrapper-inline sm">
            {!! Form::label('search', 'Search:') !!}
            {!! Form::text('search', null, ['class' => 'form-input', 'id' => 'searchInput']) !!}
      </div>
  </div>
  <div class="block-group">
    <div class="block">
      <div class="block-title"><h4>Profile:</h4></div>
        <table class="information-table">
          <thead>
            <th>No.</th>
            <th>Title</th>
            <th>Body</th>
            <th>Image</th>
            <th>Created At</th>
            <th>Action</th>
          </thead>
          <tbody id="fbody">
            @foreach ($articles as $i => $article)
              <tr>
                <td>{{ $i+1 }}</td>
                <td>{{ $article->title }}</td>
                <td>{{ str_limit($article->body, 50) }}</td>
                <td>{{ $article->image }}</td>
                <td>{{ $article->created_at }}</td>
                <td>
                  {!! Form::open(['method' => 'DELETE', 'route' => ['admin::admin.article.destroy', $article->id]]) !!}                      
                    <a href="{!! 'article/' . $article->id . '/edit'!!}" class="form-button active">Edit</a> 
                    {!! Form::submit('Delete', ['class' => 'form-button danger']) !!}
                  {!! Form::close() !!}
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
  </div>
@stop

@section('footer')

  <script>
  $("#searchInput").keyup(function () {
    //split the current value of searchInput
    var data = this.value.split(" ");
    //create a jquery object of the rows
    var row = $("#fbody").find("tr");
    if (this.value == "") {
        row.show();
        return;
    }
    //hide all the rows
    row.hide();

    //Recusively filter the jquery object to get results.
    row.filter(function (i, v) {
        var $t = $(this);
        for (var d = 0; d < data.length; ++d) {
            if ($t.is(":contains('" + data[d] + "')")) {
                return true;
            }
        }
        return false;
    })
    //show the rows that match.
    .show();
    }).focus(function () {
        this.value = "";
        $(this).unbind('focus');
    });
  </script>

@stop