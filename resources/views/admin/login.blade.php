@extends('layouts.min-app')

@section('title')
	Administrator Login
@stop

@section('contents')
	<section class="login-region">
    {!! Form::open(['url' => 'register']) !!}
        <div class="inner-right-region wow fadeInLeft animated" data-wow-delay="0.5s">
            <h4 class="section-header">Login</h4>
            <div class="field-wrapper-block lg">
                {!! Form::label('username', 'Username') !!}
                {!! Form::text('username', null, ['class' => 'form-select']) !!}
            </div>
            <div class="field-wrapper-block lg">
                {!! Form::label('password', 'Password') !!}
                {!! Form::password('password', ['class' => 'form-input']) !!}
            </div>
            <div class="field-wrapper-inline sm">
                {!! Form::label('remember_me', 'Remember Me') !!}
                {!! Form::input('checkbox', 'remember_me', null, ['class' => 'form-input']) !!}
            </div>
           
            <div class="field-wrapper-inline">
                {!! Form::submit('Agree and Open Account', ['class' => 'form-button active']) !!}
            </div>
            <div class="field-wrapper-inline">
                <a class="form-button default" href="l5agenwin/public/">Back to home</a>
            </div>
        </div>
    {!! Form::close() !!}
	</section>
@stop