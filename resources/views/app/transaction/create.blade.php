@extends('layouts.app')

@section('contents')
	<div class="subpage-titles fadeIn animated">
	  <div class="inner-subpage-titles">
	      <h2 class="subpage-title">Transaction</h2>
	  </div>
	</div>

  @include ('errors.list')

  @include('partials.flash')

	<div class="content-region">
    {!! Form::model($user->bankAccount, ['url' => 'transaction']) !!}
      <div class="inner-content-region">
        <h4 class="section-header">Transaction</h4>
        <div class="field-wrapper-block lg">
            {!! Form::hidden('trxn_type_id', '1') !!}
        </div>
        <div class="field-wrapper-block lg">
            {!! Form::label('account_name', 'Account name *') !!}
            {!! Form::text('account_name', null, ['class' => 'form-input', 'required' => 'required', 'readonly']) !!}
        </div>
        <div class="field-wrapper-block lg">
            {!! Form::label('account_no', 'Account number *') !!}
            {!! Form::text('account_no', null, ['class' => 'form-input', 'required' => 'required', 'readonly']) !!}
        </div>
        <div class="field-wrapper-block lg">
            {!! Form::label('amount', 'Amount') !!}
            {!! Form::text('amount', null, ['class' => 'form-input', 'required' => 'required']) !!}
        </div>
        <div class="field-wrapper-inline">
            {!! Form::submit('Submit', ['class' => 'form-button active']) !!}
        </div>
      </div>
    {!! Form::close() !!}
	</div>
@stop