@extends('layouts.app')

@section('contents')
	<div class="subpage-titles fadeIn animated">
	  <div class="inner-content">
	      <h2 class="subpage-title">Transaction</h2>
	  </div>
	</div>

  @include ('errors.list')
  
  @include('partials.flash')
  	<div class="content-region">
      @unless (!in_array_r('Pending', $user->transaction->toArray()) && !in_array_r('To Be Approve', $user->transaction->toArray()) && !in_array_r('Processing', $user->transaction->toArray()))
        <table class="display-table">
          <thead>
              <tr>
                  <th>Date of Withdraw</th>
                  <th>Requested Amount</th>
                  <th>Status</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>
              @foreach ($user->transaction as $transaction)
                @if ($transaction->trxn_status_id != 7 && $transaction->trxn_status_id != 4)
                  <tr>
                      <td>{{ $transaction->created_at }}</td>
                      <td>{{ number_format( abs($transaction->amount), 0 , '' , '.' ) }}</td>
                      <td>{{ $transaction->status->name }}</td>
                      <td>
                      @if ($transaction->trxn_status_id != 5 && $transaction->trxn_status_id != 6)
                          {!! Form::open(['method' => 'PATCH', 'url' => ['transaction/withdraw', $transaction->id]]) !!}
                              <div class="field-wrapper-block sm">
                                {!! Form::submit('Cancel Withdraw', ['class' => 'form-button active']) !!}
                              </div>
                          {!! Form::close() !!}
                      @endif
                      </td>
                  </tr>
                @endif
              @endforeach
          </tbody>
        </table>
      @endif
      <table class="display-table">
        <thead>
            <tr>
                <th>Date Withdraw</th>
                <th>Withdraw Amount</th>
                <th>Game Account</th>
                <th>Date Completed</th>
                <th>Status</th>
                <th>Reason</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($user->transaction as $transaction)
                <tr>
                    <td>{{ $transaction->created_at }}</td>
                    <td>{{ number_format( abs($transaction->amount), 0 , '' , '.' ) }}</td>
                    <td>{{ $transaction->gameType->name }}</td>
                    <td>{{ $transaction->updated_at }}</td>
                    <td>{{ $transaction->status->name }}</td>
                    <td>
                    @if ($transaction->status->name == 'Rejected')
                        {{ $transaction->remarks }}
                    @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
      </table>
        {!! Form::open(['url' => 'transaction']) !!}
          <div class="inner-content-region">
            <h4 class="section-header">Withdraw</h4>
        @if (!in_array_r('Pending', $user->transaction->toArray()) && !in_array_r('To Be Approve', $user->transaction->toArray()) && !in_array_r('Processing', $user->transaction->toArray()))
          @if (!$user->gameAccount->count() < 1)
            <div class="field-wrapper-block lg">
                {!! Form::hidden('trxn_type_id', '2') !!}
            </div>          
            <div class="field-wrapper-block lg">
                {!! Form::label('game_id', 'Select Game *') !!}
                {!! Form::select('game_id', $type, null, ['class' => 'form-select']) !!}
            </div>
            <div class="field-wrapper-block lg">
                {!! Form::label('amount', 'Amount') !!}
                {!! Form::text('amount', null, ['class' => 'form-input', 'required' => 'required']) !!}
            </div>
            <div class="field-wrapper-inline">
                {!! Form::submit('Submit', ['class' => 'form-button active']) !!}
            </div>
          </div>
        {!! Form::close() !!}
        @else
        <h4>Its like you don't have a game account, please deposit first.</h4>
        @endif
      @endif
	</div>
@stop