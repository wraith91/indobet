@extends('layouts.app.master')

@section('contents')
	<div class="casino-content">
        <div class="casino-space w100"></div><!-- casino-space -->
        <div class="casino-wrapper container">
            <div class="breadcrumb">
                <a href="/">
                    Home
                </a>
                &nbsp;
                <span>
                    &gt;
                </span>
                &nbsp;
                Casino
            </div><!-- breadcrumb -->
            <div class="custom-sb w100">
                <div class="casino-box-wrapper">
                    <div class="casino-box floatLeft">
                        <a href="{{ URL::to('casino-sbobet') }}">
                            <img src="{{ asset('main/app/img/logo/logo-sbobetcasino.png') }}" width="180" height="180" />
                        </a>
                    </div><!-- casino-box -->
                    <div class="casino-box floatLeft">
                        <a href="{{ URL::to('casino-ibcbet') }}">
                            <img src="{{ asset('main/app/img/logo/logo-ibcbet-2.png') }}" width="180" height="180" />
                        </a>
                    </div><!-- casino-box -->
                    <div class="casino-box floatLeft">
                        <a href="{{ URL::to('casino-calibet') }}">
                            <img src="{{ asset('main/app/img/logo/logo-calibet-color.png') }}" width="180" height="180" />
                        </a>
                    </div><!-- casino-box -->

                    <div class="clearBoth"></div><!-- clearBoth -->
                </div><!-- casino-box-wrapper -->
            </div><!-- custom-sb -->
        </div><!-- casino-wrapper -->
    </div><!-- casino-content -->
@stop
<!-- casino-box
<div class="casino-box floatLeft">
                        <a href="casino-ionclub.html">
                            <img src="{{ asset('main/app/img/logo/logo-ionclub.png') }}" width="180" height="180" />
                        </a>
                    </div>
                    <div class="casino-box floatLeft">
                        <a href="casino-guavita.html">
                            <img src="{{ asset('main/app/img/logo/logo-guavita.png') }}" width="180" height="180" />
                        </a>
                    </div>
                    <div class="casino-box floatLeft">
                        <a href="casino-starofasia.html">
                            <img src="{{ asset('main/app/img/logo/logo-starofasia.png') }}" width="180" height="180" />
                        </a>
                    </div> -->