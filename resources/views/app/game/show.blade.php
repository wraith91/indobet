@extends('layouts.app')

@section('contents')
	<div class="subpage-titles fadeIn animated">
	  <div class="inner-content">
	      <h2 class="subpage-title">Game Profile</h2>
	  </div>
	</div>
    <div class="content-region">
        <table class="display-table">
            <thead>
                <tr>
                    <th>Game</th>
                    <th>ID</th>
                    <th>Password</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($details->gameAccount as $user)
                    <tr>
                        <td>{!! $user->gameType->name  !!}</td>
                        <td>{!! $user->username  !!}</td>
                        <td>{!! $user->password  !!}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@stop