@extends('layouts.app.master')

@section('contents')
	<div class="casino-content">
        <div class="casino-space w100"></div><!-- casino-space -->
        <div class="casino-wrapper container">
            <div class="breadcrumb">
                <a href="/">
                    Home
                </a>
                &nbsp;
                <span>
                    &gt;
                </span>
                &nbsp;
                Poker
            </div><!-- breadcrumb -->
            <div class="custom-sb w100">
                <div class="poker-detail w100 taCenter positionRelative">
                    <img src="{{ asset('main/app/img/def-poker.png') }}" />
                    <div class="btn-register-now-2 taCenter positionRelative btn-register-poker">
                        <a href="/register">
                            daftar sekarang
                        </a>
                    </div><!-- btn-register-now-2 -->
                </div><!-- poker-detail -->
            </div><!-- custom-sb -->
        </div><!-- casino-wrapper -->
    </div><!-- casino-content -->
@stop
