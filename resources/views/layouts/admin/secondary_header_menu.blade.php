<div class="subpage-header">
  <h2 class="subpage-header-title">{!! $title !!}</h2>
  <nav class="subpage-header-lists">
    @if (isset($secondary_menu) && count($secondary_menu) > 0)
	    <ul>
		    @foreach ($secondary_menu as $menu_item => $link)
		    	<li><a href="{{ URL::to($link) }}" class="{{ set_active([$link]) }}">{!! ucfirst($menu_item) !!}</a></li>
		    @endforeach
	    </ul>
    @endif
  </nav>
</div>