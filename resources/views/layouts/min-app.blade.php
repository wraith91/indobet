<!doctype html>
<html class="no-js" lang="en">
    <head>
        <title> @yield('title') </title>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="betting">
        <meta name="author" content="jyerro">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" type="text/css" href="{{ asset('main/css/vendor/all.css') }}"> -->
        <link rel="stylesheet" type="text/css" href="{{ asset('main/css/styles.css') }}">
        @yield('header')

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="container">
            <header class="secondary-header">
                <div class="inner-content">
                    <div class="logo-block">
                        <a href="/"><h1 class="hide-text show-image">9betGroup</h1></a>
                    </div>
            </header>
            <main>

            @yield('contents')

            <a href="#0" class="cd-top">Top</a>
            </main>
        </div>

        <!-- // <script src="{{ asset('main/js/vendor/all.js') }}"></script> -->
        <script src="{{ asset('main/js/script1.js') }}"></script> <!-- Gem jQuery -->
        <!-- Additional Scripts  -->

        @yield('footer')

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
