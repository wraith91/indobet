<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class GameTransactionLog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'game_transaction_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['game_profile_id', 'transaction_id', 'from', 'to'];

    /**
     * User has many game accounts
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gameAccount()
    {
        return $this->belongsTo(UserGameProfile::class, 'id');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id');
    }
}
