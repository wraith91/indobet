<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    /**
   * The database table used by the model.
   *
   * @var string
   */
	protected $table = 'bonuses';

	/**
   * Fillable fields for a InquiryType
   *
   * @var array
   */
	protected $fillable = ['game_profile_id', 'game_type_id', 'bonus_status_id', 'bonus_point', 'balance', 'file_log_id'];

  /**
   * The attributes that have their default value before stored in the database.
   *
   * @var array
   */
	protected $attributes = ['bonus_status_id' => 1];

	/**
	 * Bonus is uploaded by the administrator which is belongs to the user
	 * 
	 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function gameAccount()
    {
			return $this->belongsTo(UserGameProfile::class, 'game_profile_id');
    }

    /**
     * Bonus has one status
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status()
    {
			return $this->hasOne(BonusStatus::class, 'id', 'bonus_status_id');
    }

    /**
     * Bonus from one file
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function file()
    {
      return $this->hasOne(BonusFileLog::class, 'id');
    }

    /**
     * Bonus has one game type
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function gameType()
    {
        return $this->hasOne(GameType::class, 'id', 'game_type_id');
    }

    public function scopeNotTransferred($query)
    {
        return $query->where('bonus_status_id', 1);
    }

    public function scopeTransferred($query)
    {
        return $query->where('bonus_status_id', 2);
    }

    public function scopeSbo($query)
    {
        return $query->where('game_type_id', 1);
    }

    public function scopeIbc($query)
    {
        return $query->where('game_type_id', 2);
    }

    public function scopeCali($query)
    {
        return $query->where('game_type_id', 3);
    }

    public function scopeKlik($query)
    {
        return $query->where('game_type_id', 4);
    }

    public function scopeFile($query, $search)
    {
        return $query->where('file_log_id', $search);
    }
}
