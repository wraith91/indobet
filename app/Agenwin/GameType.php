<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class GameType extends Model
{
		/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'game_types';

		/**
	   * Fillable fields for a banktype
	   *
	   * @var array
	   */
		protected $fillable = ['name'];

		/**
	   * The timestamp is disabled in this model.
	   *
	   * @var string
	   */
		public $timestamps = false;
}
