<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Transaction extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
	protected $table = 'transactions';

	/**
   * Fillable fields for a InquiryType
   *
   * @var array
   */
	protected $fillable = ['trxn_type_id', 'trxn_status_id', 'bank_id', 'game_id', 'amount', 'remarks', 'modify_by', 'completed_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['completed_at'];

    /**
     * The attributes that have their default value before stored in the database.
     *
     * @var array
     */
	protected $attributes = ['trxn_status_id' => 2];

	/**
	 * Transaction is created by the user
	 * 
	 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function user()
    {
		return $this->belongsTo(User::class);
    }

    /**
     * Transaction has one status
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status()
    {
		return $this->hasOne(TransactionStatus::class, 'id', 'trxn_status_id');
    }

    /**
     * Transaction is associated with the bank account of the user
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bank()
    {
		return $this->hasOne(UserBankProfile::class, 'id', 'bank_id');
    }

    /**
     * Transaction has one game type
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function gameType()
    {
        return $this->hasOne(GameType::class, 'id', 'game_id');
    }

    public function scopeFrom($query, $filterDate)
    {
        return $query->where('created_at', '>=', $filterDate);
    }

    public function scopeBetween($query, $from, $to)
    {
        return $query->whereBetween('created_at', [$from, $to]);
    }

    /**
     * Scope a query to only include pending transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePending($query)
    {
        return $query->where('trxn_status_id', 2);
    }

    /**
     * Scope a query to only include approved transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query)
    {
        return $query->where('trxn_status_id', 3);
    }

    /**
     * Scope a query to only include rejected transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRejected($query)
    {
        return $query->where('trxn_status_id', 4);
    }

    /**
     * Scope a query to only include to be approved transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTobeapproved($query)
    {
        return $query->where('trxn_status_id', 5);
    }

    /**
     * Scope a query to only include processing transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProcessing($query)
    {
        return $query->where('trxn_status_id', 6);
    }

    /**
     * Scope a query to only include cancelled transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCancelled($query)
    {
        return $query->where('trxn_status_id', 7);
    }

    /**
     * Scope a query to only include deposit transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDeposit($query)
    {
        return $query->where('trxn_type_id', '1');
    }

    /**
     * Scope a query to only include withdraw transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithdraw($query)
    {
        return $query->where('trxn_type_id', '2');
    }

    /**
     * Set the completed date format
     *
     * @param  string  $value
     * @return string
     */
    public function setCompletedAtAttribute($date)
    {
        $this->attributes['completed_at'] = Carbon::createFromFormat('Y-m-d', $date);
    }
}
