<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'articles';
    
    /**
     * Fillable fields for the model
     *
     * @var array
     */
    protected $fillable = ['title', 'body', 'image', 'active', 'published_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['published_at'];

	/**
	 * Article is created by the user
	 * 
	 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function user()
    {
		return $this->belongsTo(User::class);
    }
}

