<?php

namespace App\Http\Controllers;

use App\Agenwin\Transaction;
use App\Agenwin\UserGameProfile;
use App\Agenwin\GameTransactionLog;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class GameTransactionLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logs = GameTransactionLog::with('transaction.user')->orderBy('created_at', 'desc')->get();

        $i = 1;

        return view('admin.log.index', compact('logs', 'i'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $transaction = Transaction::select('amount')->find($request->transaction_id);

        $total = $request->from - $request->to;

        if ($total != abs($transaction->amount))
        {
            return redirect()->back()->withErrors('Invalid difference from transaction log.');
        }

        $gameProfile = UserGameProfile::with('transactionLogs')->find($id);

        $transactionLog = new GameTransactionLog($request->except('_token'));

        if ($gameProfile->transactionLogs()->save($transactionLog))
        {
            session()->flash('flash_message', 'Withdrawal log has been created.');
        }

        return redirect()->back();
    }
}
