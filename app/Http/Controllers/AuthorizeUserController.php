<?php

namespace App\Http\Controllers;

use App\Agenwin\Role;
use App\Agenwin\User;
use App\Agenwin\UserProfile;

use Illuminate\Http\Request;
use App\Http\Requests\SystemUserRegistrationRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AuthorizeUserController extends Controller
{
    protected $user;
    protected $role;

    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->user->whereHas('roles', function ($query) {
                                $query->whereIn('name', ['admin', 'cs']);
                            })->get();
        
        $i = 1;

        return view('admin.system.index', compact('users', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->role->select(DB::raw("CONCAT(name, ' - ', description) AS label, id"))->lists('label', 'id');

        return view('admin.system.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SystemUserRegistrationRequest $request)
    {
        // return $request->all();
        $input = $request->except('_token', 'role'); //validation of inputs

        $role = $this->role->find($request->role);

        $user = new User([
            'username'  => $input['username'],
            'password'  => $input['password'],
            'email'     => $input['email'],
        ]);

        $profile = new UserProfile([
            'first_name' => $input['first_name'],
            'last_name'  => $input['last_name'],
            'gender'     => $input['gender'],
            'mobile'     => $input['mobile'],
        ]);

        $user->save();

        $profile = $user->profile()->save($profile);
   
        if ($user->assignRole($role->name))
        {
            session()->flash('flash_message', 'New ' . strtoupper($role->name) . ' account sucessfully created!');
        }

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user->with('profile')->find($id);

        $roles = $this->role->select(DB::raw("CONCAT(name, ' - ', description) AS label, id"))->lists('label', 'id');
       
        return view('admin.system.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->user->with('profile')->find($id);

        $input = $request->except('_method', '_token', 'role');

        if ($user->fill($input)->save() && $user->profile->fill($input)->save())
        {
            session()->flash('flash_message', 'Account successfully updated!');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->user->findOrFail($id);

        if ($user->delete())
        {
            session()->flash('flash_message', $user->username . ' has been sucessfully deleted!');
        }

        return redirect()->back();
    }
}
