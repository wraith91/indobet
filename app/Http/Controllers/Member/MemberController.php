<?php

namespace App\Http\Controllers\Member;

use App\Agenwin\User;
use App\Agenwin\Role;
use App\Agenwin\GameType;
use App\Agenwin\Transaction;
use App\Agenwin\GameTransactionLog;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MemberController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request = null)
    {
        if ($request->get('search'))
        {
            try
            {
                $user = $this->user->with('profile', 'gameAccount.gameType', 'transaction')->whereHas('roles', function ($query) {
                                        $query->where('name', 'member');
                                    })->search(trim($request->get('search')))->firstOrFail();

                $deposits = $user->transaction()->deposit()->get();

                $withdraws = $user->transaction()->withdraw()->get();
            }
            catch (ModelNotFoundException $e) 
            {
                return redirect()->intended('admin/members/information')->withErrors('No information was found...');
            }

            if (!count($user->transaction) < 1) {
                $balance = $user->transaction->sum('amount');

                $max_balance = $user->transaction->where('trxn_status_id', '3')->where('trxn_type_id', '1')->sum('amount');

                $latest_trxn = $user->transaction()->orderBy('created_at', 'desc')->first();

                $latest_trxn = $latest_trxn->created_at;
            }
            else {
                $balance = 0;

                $max_balance = 0;

                $latest_trxn = collect(['created_at' => 'No transaction created...']);

                $latest_trxn = $latest_trxn->get('created_at');
            }
        }

        $game = GameType::lists('name', 'id');

        return view('admin.member.index', compact('user', 'balance', 'max_balance', 'latest_trxn', 'deposits', 'withdraws', 'game'));
    }

    public function show($name, $id, GameTransactionLog $game_transaction_log)
    {
        $logs = $game_transaction_log->where('game_profile_id', $id)->get();

        return view('admin.member.show', compact('logs'));
    }

    public function edit($id)
    {
        $user = $this->user->with('profile', 'bankAccount')->find($id);

        return view('admin.member.changepass.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, ['password' => 'required|min:8', 'confirm_password' => 'required|same:password']);

        $user = $this->user->find($id);

        if ($user->fill($request->only('password'))->save())
        {
            session()->flash('flash_message', 'Member password has been successfully changed!');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->user->find($id);

        if ($user->delete())
        {
            session()->flash('flash_message', $user->username . ' account has been successfully deleted!');
        }

        return redirect()->back();
    }

    public function lists(Request $request)
    {
        $users = $this->user->with('bankAccount', 'bankAccount.bankName')->whereHas('roles', function ($query) {
                                        $query->where('name', 'member');
                                    })->get();

        return view('admin.member.lists', compact('users'));
    }
}
