<?php

namespace App\Http\Controllers\Member;

use App\Agenwin\User;
use App\Agenwin\UserBankProfile;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MemberBankAccountController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['account_name' => 'required', 'account_no' => 'required']);

        $user = User::find($id);

        if ($user->bankAccount->fill($request->except('_method', '_token'))->save())
        {
            session()->flash('flash_message', 'Member bank account details has been successfully changed!');
        }

        return redirect()->back();
    }
}
