<?php

namespace App\Http\Controllers;

use App\Agenwin\Inquiry;
use App\Agenwin\User;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user, Inquiry $inquiry)
    {
        $inquiry = $inquiry->where('inquiry_status_id', 1)
                            ->count();

        $total_members = $user->whereHas('roles', function ($query){
                    $query->where('name', 'member');
                })->count();

        $new_members = $user->whereHas('roles', function ($query){
                             $query->where('name', 'member');
                        })->where('created_at', '>=', Carbon::now()->startOfMonth())
                          ->where('created_at', '<=', Carbon::now()->endOfMonth())
                          ->count();

        $active_members = $user->whereHas('roles', function ($query){
                    $query->where('name', 'member');
                })->where('is_active', '1')->count();

        return view('admin.dashboard.index', compact('inquiry', 'total_members', 'new_members', 'active_members'));
    }
}
