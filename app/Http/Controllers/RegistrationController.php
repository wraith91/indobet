<?php

namespace App\Http\Controllers;

use App\Agenwin\User;
use App\Agenwin\UserProfile;
use App\Agenwin\UserBankProfile;
use App\Agenwin\BankType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegistrationRequest;
use Auth;


class RegistrationController extends Controller
{
    /**
     * Display registration page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bank_name = BankType::lists('name', 'id');

        return view('app.register.create', compact('bank_name'));
    }

    /**
     * Store a newly created user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegistrationRequest $request)
    {
        $user = new User($request->only('username', 'password', 'email'));

        $profile = new UserProfile($request->only('first_name', 'mobile', 'birth_date'));

        $bank = new UserBankProfile($request->only('bank_type_id', 'account_name', 'account_no'));

        $user->save();

        $profile = $user->profile()->save($profile);

        $bank = $user->bankAccount()->save($bank);
        
        $user->assignRole('member');    // assign a member role before logging in

        Auth::login($user);

        return redirect()->route('home');
    }

}
