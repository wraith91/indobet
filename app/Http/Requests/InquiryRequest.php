<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InquiryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (auth()->check())
        {
            return [
                'subject' => 'required',
                'body' => 'required',
                'g-recaptcha-response'  => 'required'
            ];
        }
        return [
                'name' => 'required',
                'email' => 'required|email',
                'subject' => 'required',
                'body' => 'required',
                'g-recaptcha-response'  => 'required'
            ];
    }
}
