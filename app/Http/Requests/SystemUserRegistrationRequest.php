<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SystemUserRegistrationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'         => 'required|unique:users',
            'email'            => 'required|email|unique:users',
            'password'         => 'required|min:8',
            'confirm_password' => 'required|same:password',
            'first_name'       => 'required',
            'last_name'        => 'required'
        ];
    }
}
