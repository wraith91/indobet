<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Input;

class RegistrationRequest extends Request
{
    protected $rules = [
        'username'         => 'required|unique:users',
        'email'            => 'required|email|unique:users',
        'password'         => 'required|min:8',
        'confirm_password' => 'required|same:password',
        // 'first_name'    => 'required',
        // 'last_name'     => 'required',
        // 'birth_date'       => 'required',
        'mobile'           => 'required|min:10',
        'bank_type_id'     => 'required',
        'account_name'     => 'required',
        'account_no'       => 'required|string|unique:user_bank_profiles,account_no|size:10'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
        
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = $this->rules;

        if (is_numeric($this->request->get('account_no')))          //check if the input is numeric
        {
            if ($this->request->get('bank_type_id') == 4)              //check what bank is submitted
            {
                $rules['account_no'] = 'required|string|unique:user_bank_profiles,account_no|size:13';   //apply custom rule for each bank
            }
            else if ($this->request->get('bank_type_id') == 2)
            {
                $rules['account_no'] = 'required|string|unique:user_bank_profiles,account_no|size:15';
            }
        }

        return $rules;
    }

    public function messages()
    {   
        return $messages = [
            'account_no.size'    => 'The :attribute must be :size numbers.', // custom message for required rule.
            'account_no.unique'    => 'The :attribute must be valid.', // custom message for required rule.
        ];     
    }
}
