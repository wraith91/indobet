<?php

namespace App\Http\Middleware;

use Closure;

class AbortIfNotCompanyIp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if ($_SERVER['REMOTE_ADDR'] != '203.82.35.74' && $_SERVER['REMOTE_ADDR'] != '124.105.171.150')
        // {
        //     abort(403, 'Unauthorized action.');
        // }

        return $next($request);
    }
}
